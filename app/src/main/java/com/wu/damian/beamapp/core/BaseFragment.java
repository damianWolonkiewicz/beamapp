package com.wu.damian.beamapp.core;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.wu.damian.beamapp.navigation.NavigationInterface;

/** Provide access to context for each Fragment  */
public class BaseFragment extends Fragment {

    private static final String TAG = "mINFO";
    private NavigationInterface navigation = null;

    protected NavigationInterface getNavigation() {
        return navigation;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            Log.d(TAG, "onAttach: BaseFragment called !");
            navigation = (NavigationInterface) context;
        } catch (Exception ex) {
            throw new IllegalStateException("Root activity don't implement correct navigation interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        navigation = null;
    }
}
