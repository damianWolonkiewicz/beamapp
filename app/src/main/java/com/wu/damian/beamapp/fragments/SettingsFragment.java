package com.wu.damian.beamapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.utils.QuerySettings;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SettingsFragment extends BaseFragment {

    @BindView(R.id.checkBox_individual)
    CheckBox checkBoxIndividual;
    @BindView(R.id.checkBox_professions)
    CheckBox checkBoxProfessions;
    Unbinder unbinder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);

        readStartSettings();
        initListeners();

        return view;
    }

    private void initListeners() {
        checkBoxIndividual.setOnCheckedChangeListener((buttonView, isChecked) ->
                QuerySettings.setStoredSettingsIndividual(getContext(), isChecked));

        checkBoxProfessions.setOnCheckedChangeListener((buttonView, isChecked) ->
                QuerySettings.setStoredSettingsProfession(getContext(), isChecked));
    }

    private void readStartSettings() {
        checkBoxIndividual.setChecked(QuerySettings.getStoredSettingsIndividual(getContext()));
        checkBoxProfessions.setChecked(QuerySettings.getStoredSettingsProfession(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
