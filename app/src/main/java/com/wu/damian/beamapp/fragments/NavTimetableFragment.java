package com.wu.damian.beamapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.utils.BundleKey;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NavTimetableFragment extends BaseFragment {

    @BindView(R.id.nav_fragment_layout)
    ConstraintLayout navFragmentLayout;
    @BindView(R.id.bottomNavigationView)
    BottomNavigationView bottomNavigationView;
    Unbinder unbinder;
    private String dayName;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nav_timetable, container, false);
        unbinder = ButterKnife.bind(this, view);
        initVariables();
        initListeners();


        return view;
    }

    private void initVariables() {
        Bundle bundle = null;
        // Receive notification from previous fragment
        if (getArguments() != null) {
            bundle = getArguments();
            // Day name && profession received from prev Fragment.
            dayName = bundle.getString(BundleKey.Info.dayName);
            bundle.putString(BundleKey.Info.navDestination, "profession");
            Log.d(MainActivity.TAG, "initVariables: bundle called dayName : " + dayName);
        }


        getNavigation().getFragmentManagerV4()
                .beginTransaction()
                .replace(R.id.nav_fragment_layout, TimetableFragment.getInstance(bundle), null).commit();
    }

    private void initListeners() {
        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            Fragment fragment;
            Bundle bundle = new Bundle();
            bundle.putString(BundleKey.Info.dayName, dayName);

            switch (menuItem.getItemId()) {
                case R.id.mn_profession:
                    bundle.putString(BundleKey.Info.navDestination, "profession");
                    fragment = TimetableFragment.getInstance(bundle);
                    break;
                case R.id.mn_individual:
                    bundle.putString(BundleKey.Info.navDestination, "individual");
                    fragment = TimetableFragment.getInstance(bundle);
                    break;
                default:
                    fragment = new TimetableFragment();
            }

            getNavigation().getFragmentManagerV4()
                    .beginTransaction()
                    .replace(R.id.nav_fragment_layout, fragment, null).commit();

            return true;
        });
    }

    public static NavTimetableFragment getInstance(Bundle bundle) {
        NavTimetableFragment tf = new NavTimetableFragment();
        tf.setArguments(bundle);
        return tf;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
