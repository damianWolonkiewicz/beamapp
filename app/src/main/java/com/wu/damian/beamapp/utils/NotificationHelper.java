package com.wu.damian.beamapp.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;

public class NotificationHelper {

    public static void showNotification(String title, String body, Context context) {
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel nc = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NotificationChannelBeamApp",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            nc.setDescription("Description - BeamApp");
//            nc.enableLights(true);
//            nc.setLightColor(Color.BLUE);
//            nc.setVibrationPattern(new long[]{0, 1000, 500, 1000});
//            nc.enableLights(true);
//            notificationManager.createNotificationChannel(nc);
//        }

        /** Open app after notification click */
//        Intent intent = new Intent(context, MainActivity.class);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(
//                context,
//                100,
//                intent,
//                PendingIntent.FLAG_CANCEL_CURRENT
//        );

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, MainActivity.NOTIFICATION_CHANNEL_ID);
        notificationBuilder
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_about_app)
//                .setContentIntent(pendingIntent)
                .setVibrate(new long[]{0, 1000, 500, 1000})
                .setContentTitle(title)
                .setContentText(body)
                .setContentInfo("BeamApp");
        NotificationManagerCompat notifyManager = NotificationManagerCompat.from(context);
        notifyManager.notify(1, notificationBuilder.build());
    }
}
