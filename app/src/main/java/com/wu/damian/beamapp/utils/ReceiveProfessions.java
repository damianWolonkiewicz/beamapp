package com.wu.damian.beamapp.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wu.damian.beamapp.database.interfaces.FirebaseCallback;
import com.wu.damian.beamapp.database.model.Profession;
import com.wu.damian.beamapp.database.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.wu.damian.beamapp.MainActivity.TAG;

public class ReceiveProfessions {

    public static void receiveProfessions(FirebaseCallback firebaseCallback) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Professions");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> professions = new ArrayList<>();
                for (DataSnapshot ds :
                        dataSnapshot.getChildren()) {
                    professions.add(Objects.requireNonNull(ds.getValue(Profession.class)).getName());
                }
                    firebaseCallback.onCallback(professions);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static void receiveUsers(FirebaseCallback firebaseCallback) {
        DatabaseReference dr = FirebaseDatabase.getInstance().getReference().child("Users");
        dr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> users = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren())
                    users.add(Objects.requireNonNull(ds.getValue(User.class)).getName());
                firebaseCallback.onCallback(users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
    }


}
