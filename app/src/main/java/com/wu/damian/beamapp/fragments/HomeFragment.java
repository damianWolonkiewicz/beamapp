package com.wu.damian.beamapp.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.adapters.WeekdayAdapter;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.utils.NavDrawerContent;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class HomeFragment extends BaseFragment {

    @BindView(R.id.week_day_recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.textView_token)
    TextView textViewToken;
    private Unbinder unbinder;
    private WeekdayAdapter weekdayAdapter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        unlockDrawer();
        initAdapter();
        initRecycler();

        if (savedInstanceState == null)
            NavDrawerContent.setNavDrawerContent(getActivity());

        return view;
    }

    // Setting navigation drawer correct for logged in user profession


    private void unlockDrawer() {
        DrawerLayout drawer = getActivity().findViewById(R.id.main_drawer_layout);
        // Unblock Navigation drawer in !LoginFragment
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void initRecycler() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(weekdayAdapter);
    }

    private void initAdapter() {
        ArrayList<String> daysOfWeek = initWeekDays();
        weekdayAdapter = new WeekdayAdapter();
        weekdayAdapter.setNavigationInterface(getNavigation());
        weekdayAdapter.setData(daysOfWeek);
    }

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    public static HomeFragment getInstance(Bundle bundle) {
        HomeFragment hf = new HomeFragment();
        hf.setArguments(bundle);
        return hf;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private ArrayList<String> initWeekDays() {
        ArrayList<String> dow = new ArrayList<>();
        dow.add("Monday");
        dow.add("Tuesday");
        dow.add("Wednesday");
        dow.add("Thursday");
        dow.add("Friday");
        dow.add("Saturday");
        dow.add("Sunday");
        return dow;
    }

}
