package com.wu.damian.beamapp.adapters.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.wu.damian.beamapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfessionViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textView_rv_profession)
    public TextView professionName;

    public ProfessionViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
