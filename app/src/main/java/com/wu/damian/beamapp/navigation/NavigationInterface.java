package com.wu.damian.beamapp.navigation;

import android.support.v4.app.FragmentManager;
import com.wu.damian.beamapp.core.BaseFragment;

public interface NavigationInterface {

    void changeFragment(BaseFragment fragment);
    void changeFragment(BaseFragment fragment, boolean addToBackStack);
    void changeFragment(BaseFragment fragment, boolean addToBackStack, boolean withNavigation);
    FragmentManager getFragmentManagerV4();
}
