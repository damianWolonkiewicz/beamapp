package com.wu.damian.beamapp.database.model;

public class User {
    private String name;
    private String email;
    private String profession;
    private String uid;


    public User() {
    }

    public User(String name, String email, String profession, String uid) {
        this.name = name;
        this.email = email;
        this.profession = profession;
        this.uid = uid;

    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
