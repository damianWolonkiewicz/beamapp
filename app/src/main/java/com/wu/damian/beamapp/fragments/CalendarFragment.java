package com.wu.damian.beamapp.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.adapters.EmployeeAdapter;
import com.wu.damian.beamapp.adapters.EventTypeAdapter;
import com.wu.damian.beamapp.calendar.CalendarCustomView;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.database.interfaces.FirebaseCallback;
import com.wu.damian.beamapp.database.model.Event;
import com.wu.damian.beamapp.database.model.User;
import com.wu.damian.beamapp.notifications.APIService;
import com.wu.damian.beamapp.notifications.Client;
import com.wu.damian.beamapp.notifications.MyResponse;
import com.wu.damian.beamapp.notifications.Notification;
import com.wu.damian.beamapp.notifications.Sender;
import com.wu.damian.beamapp.notifications.Token;
import com.wu.damian.beamapp.utils.BundleKey;
import com.wu.damian.beamapp.utils.PushEmail;
import com.wu.damian.beamapp.utils.StateVO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wu.damian.beamapp.MainActivity.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends BaseFragment {
    @BindView(R.id.secondary_fragment)
    LinearLayout secondaryFragment;
    @BindView(R.id.activity_custom_calendar_m)
    ScrollView activityCustomCalendar;
    @BindView(R.id.custom_calendar)
    CalendarCustomView customCalendar;
    @BindView(R.id.date_range_layout)
    ConstraintLayout dateRangeLayout;
    @BindView(R.id.task_start_day)
    TextView taskStartDay;
    @BindView(R.id.text_from)
    TextView textFrom;
    @BindView(R.id.text_to)
    TextView textTo;
    @BindView(R.id.task_end_day)
    TextView taskEndDay;
    @BindView(R.id.spinner_event_type)
    Spinner spinnerEventType;
    @BindView(R.id.button_accept)
    Button buttonAccept;
    @BindView(R.id.editText_event_message)
    EditText editTextEventMessage;
    @BindView(R.id.view_text_event)
    View viewTextEvent;
    @BindView(R.id.view_event_employee)
    View viewEventEmployee;
    @BindView(R.id.view_employee_desc)
    View viewEmployeeDesc;
    @BindView(R.id.switch_send_email)
    Switch switchSendEmail;
    @BindView(R.id.switch_send_sms)
    Switch switchSendSms;
    private Spinner spinnerUsers;
    private Unbinder unbinder;
    private boolean startDateChosen;
    private Date firstDay;
    private Date secondDay;
    private View view;
    // Previous clicked cell
    private View prevCell;
    private List<User> users;
    // API
    private APIService apiService;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_calendar, container, false);
        unbinder = ButterKnife.bind(this, view);

        initVars();
        initListeners();
        initEventSpinner();

        // API
        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        return view;
    }

    // Inflate NavDrawer with content correct for logged user
    private void initEventSpinner() {
        int userType;
        if (!LoginFragment.user.getProfession().equals("Admin") && !LoginFragment.user.getProfession().equals("Manager"))
            userType = R.array.event_type_non_manager;
        else userType = R.array.event_type;
        List<String> events = new ArrayList<>(Arrays.asList(getResources().getStringArray(userType)));
        EventTypeAdapter eventTypeAdapter = new EventTypeAdapter(getContext(), 0, events);
        spinnerEventType.setAdapter(eventTypeAdapter);
    }

    // Read Users/Employees from database
    private void loadSpinnerUserContent(FirebaseCallback firebaseCallback) {
        List<User> users = new ArrayList<>();
        // Receive list of users from Firebase
        // Firebase reference
        DatabaseReference dr = FirebaseDatabase.getInstance().getReference().child("Users");
        dr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren())
                    users.add(ds.getValue(User.class));
                firebaseCallback.onCallback(users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
    }

    @SuppressLint("DefaultLocale")
    private void initListeners() {

        customCalendar.mCalendarGridView.setOnItemLongClickListener((adapterView, view, i, l) -> {
            // Hide mView of Fragment with RecyclerView
            secondaryFragment.setVisibility(View.GONE);
            // Day selected by user in Date object.
            Date date = (Date) customCalendar.mAdapter.getItem(i);
            Calendar calendar = Calendar.getInstance();
            // Day selected by user in Calendar object.
            calendar.setTime(date);

            // If first date will be chosen
            if (!startDateChosen)
                setFirstData(date, calendar);
            else {
                secondDay = date;
                // First date before/equals second date
                if (firstDay.before(secondDay) || firstDay.equals(secondDay)) {
                    // Receiving list of Employees/Users from Firebase
                    loadSpinnerUserContent(receivedUsers -> {
                        users = (List<User>) receivedUsers;
                        // List of objects displayed in spinner
                        List<StateVO> listVOs = new ArrayList<>();
                        // Set first info record
                        StateVO svo = new StateVO();
                        svo.setTitle("Select employee");
                        listVOs.add(svo);
                        // Set 'Add all' option
                        svo = new StateVO();
                        svo.setTitle("Add ALL");
                        listVOs.add(svo);
                        for (User user : users) {
                            svo = new StateVO();
                            svo.setTitle(user.getName());
                            svo.setSelected(false);
                            listVOs.add(svo);
                        }
                        // Add two empty records (first - 'Select employee' record , second - Add all employees option)
                        users.add(0, new User());
                        users.add(1, new User());
                        EmployeeAdapter myAdapter = new EmployeeAdapter(getContext(), 0, listVOs);
                        spinnerUsers.setAdapter(myAdapter);
                    });
                    setSecondData(getStringData(calendar));
                } else wrongIntervalToast();
            }
            return true;
        });

        // Single grid cell listener (each day cell)
        customCalendar.mCalendarGridView.setOnItemClickListener((parent, clickedView, position, id) -> {

            // Clear view of previous chosen cell
            prevCurrentCellLogic(parent, clickedView);

            // If RecyclerView is on screen
            secondaryFragment.setVisibility(View.VISIBLE);
            clearAddAfterEventView();
            startDateChosen = false;

            // Displaying list of events for selected day
            Date date = (Date) customCalendar.mAdapter.getItem(position);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            // First part of path to event
            String monthYear = String.format("%02d%d", calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
            // Second part of path to event
            String day = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));

            Bundle bundle = new Bundle();
            bundle.putString(BundleKey.Info.monthYear, monthYear);
            bundle.putString(BundleKey.Info.day, day);
            RVCalendarFragment rvcf = new RVCalendarFragment();
            rvcf.setArguments(bundle);
            FragmentManager fm = getNavigation().getFragmentManagerV4();
            fm.beginTransaction().replace(R.id.secondary_fragment, rvcf).commit();
        });

        buttonAccept.setOnClickListener(view -> {

            // Receiving notification entered by user
            String eventType = spinnerEventType.getSelectedItem().toString();
            String message = editTextEventMessage.getText().toString();

            if (eventType.equals("Event type")) {
                Toast.makeText(getContext(), "Please choose event type.", Toast.LENGTH_LONG).show();
                return;
            }

            if (message.equals("")) {
                Toast.makeText(getContext(), "Please add description.", Toast.LENGTH_LONG).show();
                return;
            }
            // If event is chosen && description is entered

            // Final List of users
            List<User> userChosen = new ArrayList<>();

//               If 'All users' option was NOT chosen
            if (!EmployeeAdapter.listState.get(1).isSelected()) {
                for (int i = 2; i < EmployeeAdapter.listState.size(); i++)
                    if (EmployeeAdapter.listState.get(i).isSelected())
                        userChosen.add(users.get(i));
            } else {
                // TODO - Is necessary to push all users to database? Or just set TextView to "All employees"
                // If 'All employees' option was chosen
//                    for (int i = 2; i < EmployeeAdapter.listState.size(); i++)
//                        userChosen.add(users.get(i));
                User u = new User();
                u.setName("All employees");
                userChosen.add(u);
            }

            // If no user has been selected
            if (userChosen.size() == 0) {
                Toast.makeText(getContext(), "Please choose users associated with event.", Toast.LENGTH_LONG).show();
                // Break function work
                return;
            }

            // Getting day difference
            long dayDiff = getDayDiff();
            // Creating instance of calendar and setting first day chosen by user
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(firstDay);

            // Button accept disappear
            buttonAccept.setVisibility(View.GONE);
            // Creating new event object for push to Firebase
            Event event = new Event(message, eventType, userChosen);
            // Database references
            DatabaseReference eventsReference = FirebaseDatabase.getInstance()
                    .getReference().child("Events");
            // Generate unique event id
            String uniqueKey = eventsReference.push().getKey();
            // In SB will be stored path
            StringBuilder sBuilder = new StringBuilder();
            // Paths which will be updated
            Map<String, Object> updatedEventData = new HashMap<>();

            for (int i = 0; i < dayDiff; i++) {
                // Creating [month-year/day/uniqueEventId] path
                sBuilder.append(String.format("%02d", calendar.get(Calendar.MONTH) + 1))
                        .append(String.format("%02d", calendar.get(Calendar.YEAR)))
                        .append("/")
                        .append(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)))
                        .append("/")
                        .append(uniqueKey);
                // Add event to single day
                updatedEventData.put(sBuilder.toString(), event);
                // Clear StringBuilder object
                sBuilder.setLength(0);
                // Set next day in calendar
                calendar.add(Calendar.DAY_OF_MONTH, 1);
            }

            // Do a deep-path notification push/update
            eventsReference.updateChildren(updatedEventData, (firebaseError, firebase) -> {
                if (firebaseError != null)
                    Log.d(MainActivity.TAG, "initListeners: Error updating database: "
                            + firebaseError.getMessage());
            });

            // If switch is checked email message will be send
            if (switchSendEmail.isChecked()) {
                // Get users email addresses
                String[] emails = new String[userChosen.size()];
                for (int i = 0; i < userChosen.size(); i++)
                    emails[i] = userChosen.get(i).getEmail();
                sendEmail(emails, eventType, message);
            }

            // If user checked 'Send SMS' option
            if (switchSendSms.isChecked())
                sendSMS(eventType, message);

            Toast.makeText(getContext(), "Event added successfully!", Toast.LENGTH_LONG).show();

            // Clear mView after add event
            clearAddAfterEventView();

            // Refresh mView of calendar (displaying event red dot)
            customCalendar.invalidate();

            // Build notification day info
            // Start day
            calendar.setTime(firstDay);
            sBuilder.setLength(0);
            sBuilder.append(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))).append("/")
                    .append(String.valueOf(calendar.get(Calendar.MONTH) + 1)).append("/")
                    .append(String.valueOf(calendar.get(Calendar.YEAR)));

            // End day
            calendar.setTime(secondDay);
            sBuilder.append(" - ")
                    .append(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))).append("/")
                    .append(String.valueOf(calendar.get(Calendar.MONTH) + 1)).append("/")
                    .append(String.valueOf(calendar.get(Calendar.YEAR)));

            /* Send notification(s) */
            for (int i = 0; i < userChosen.size(); i++)
                sendNotification(userChosen.get(i), Objects.requireNonNull(LoginFragment.user).getName(),
                        eventType, sBuilder.toString());

        });
    }

    private void sendNotification(User receiver, String username, String topic, String date) {
        DatabaseReference tokens = FirebaseDatabase.getInstance()
                .getReference("Tokens");

        /*   IMPORTANT   */
        /* Every token is connected with specific device, NOT user !!! */

        // Get query from Firebase with selected user token
        Query query = tokens.orderByKey().equalTo(receiver.getUid());

        // Listener only for received token from chosen employee
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: count : " + ds.getChildrenCount());

                    // Receive token from chosen employee
                    Token token = ds.getValue(Token.class);

                    Log.d(TAG, "onDataChange: token : " + Objects.requireNonNull(token).getToken());

                    // Creating new notification
                    Notification notification = new Notification(R.drawable.ic_notif_icon,
                            username + ": " + topic + " - " + date, "BeamApp - New Event");

                    Sender sender = new Sender(notification, Objects.requireNonNull(token).getToken());

                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(@NonNull Call<MyResponse> call, @NonNull Response<MyResponse> response) {
                                    // Request has succeeded ?
                                    if (response.code() == 200) {
                                        if (Objects.requireNonNull(response.body()).success != 1)
                                            Toast.makeText(getContext(), "Notif sending failed!", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(@NonNull Call<MyResponse> call, @NonNull Throwable t) {

                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendSMS(String eventType, String message) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.putExtra("sms_body", "Event: " + eventType + "\n\n" + message + "\n\nBest regards,\nManager");
        sendIntent.setType("vnd.android-dir/mms-sms");
        startActivity(sendIntent);
    }

    private void prevCurrentCellLogic(AdapterView<?> parent, View clickedView) {
        // Change previous cell color to white
        if (prevCell != null)
            prevCell.setBackground(parent.getResources().getDrawable(R.drawable.rounded_shape_transparent));
        // Set selected cell color to blue
        clickedView.setBackground(parent.getResources().getDrawable(R.drawable.rounded_shape_vacation));
        prevCell = clickedView;
    }

    private long getDayDiff() {
        // Get difference between two days (in milliseconds)
        long dayDiff = secondDay.getTime() - firstDay.getTime();
        // Converting milliseconds to days
        dayDiff = TimeUnit.DAYS.convert(dayDiff, TimeUnit.MILLISECONDS) + 1;
        return dayDiff;
    }

    private void clearAddAfterEventView() {
        setSecondDateVisibility(View.GONE);
        textFrom.setText("");
        textTo.setText("");
        taskStartDay.setText("");
        taskEndDay.setText("");
        editTextEventMessage.setText("");
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    private StringBuilder getStringData(Calendar calendar) {
        StringBuilder fullData;
        fullData = new StringBuilder();
        fullData.append(String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))).append(".")
                .append(String.format("%02d", calendar.get(Calendar.MONTH) + 1)).append(".")
                .append(calendar.get(Calendar.YEAR));
        return fullData;
    }

    private void setFirstData(Date date, Calendar calendar) {
        firstDay = date;
        // Setting "From" in TextView
        textFrom.setText(getResources().getText(R.string.day_from));
        // Setting fullData in TextView
        taskStartDay.setText(getStringData(calendar));
        // Clear sec day text (if selecting new interval)
        textTo.setText("");
        // Clear sec day text (if selecting new interval)
        taskEndDay.setText("");
        // First date chosen
        startDateChosen = true;
        runAnimation(textFrom);
        runAnimation(taskStartDay);
        // Views gone (if selecting another interval)
        setSecondDateVisibility(View.GONE);
    }

    private void setSecondData(StringBuilder dayTask) {
        taskEndDay.setText(dayTask);
        textTo.setText(getResources().getText(R.string.day_to));
        setSecondDateVisibility(View.VISIBLE);
        startDateChosen = false;
    }

    private void sendEmail(String[] users, String subject, String text) {
        String message = "Dear Friend(s),\n\n" + text +
                "\n\nBest regards,\n" + LoginFragment.user.getName();
        PushEmail.sendEmail(users, subject, message, Objects.requireNonNull(getContext()));
    }

    private void setSecondDateVisibility(int visibility) {
        spinnerUsers.setVisibility(visibility);
        buttonAccept.setVisibility(visibility);
        editTextEventMessage.setVisibility(visibility);
        spinnerEventType.setVisibility(visibility);
        viewEmployeeDesc.setVisibility(visibility);
        viewEventEmployee.setVisibility(visibility);
        viewTextEvent.setVisibility(visibility);
        switchSendEmail.setVisibility(visibility);
        switchSendSms.setVisibility(visibility);
    }

    private void runAnimation(TextView tv) {
        Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.anim_in);
        tv.startAnimation(a);
    }

    // If user choose wrong dates interval.
    private void wrongIntervalToast() {
        Toast toast = Toast.makeText(getContext(), "Wrong dates interval. Please choose second date again.", Toast.LENGTH_LONG);
        View toastView = toast.getView();
        TextView text = toastView.findViewById(android.R.id.message);
        text.setTextColor(getResources().getColor(R.color.colorToastWarning));
        toast.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void initVars() {
        spinnerUsers = view.findViewById(R.id.spinner_users);
    }

    public static CalendarFragment getInstance() {
        return new CalendarFragment();
    }

    public CalendarFragment() {
        // Required empty public constructor
    }
}
