package com.wu.damian.beamapp.utils;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.notifications.Token;

import java.util.Objects;

public class UpdateToken {
    public static void update(String token) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token token1 = new Token(token);
        Log.d(MainActivity.TAG, "updateToken: token : " + token1.getToken());
        reference.child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).setValue(token1);
    }
}
