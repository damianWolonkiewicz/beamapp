package com.wu.damian.beamapp.fragments.admin;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.adapters.viewholders.EventViewHolder;
import com.wu.damian.beamapp.adapters.viewholders.ProfessionViewHolder;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.database.model.Profession;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ManageProfessionFragment extends BaseFragment {

    @BindView(R.id.edit_profession_name)
    EditText mEditProfessionName;
    @BindView(R.id.button_add_new_profession)
    Button mButtonAddNewProfession;
    @BindView(R.id.rv_professions)
    RecyclerView mRvProfessions;
    Unbinder unbinder;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_profession, container, false);
        unbinder = ButterKnife.bind(this, view);
        initListeners();
        initRecycler();

        return view;
    }

    private void initRecycler() {
        mRvProfessions.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseRecyclerOptions<Profession> options = new FirebaseRecyclerOptions.Builder<Profession>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("Professions"), Profession.class).build();

        FirebaseRecyclerAdapter<Profession, ProfessionViewHolder> mFra = new FirebaseRecyclerAdapter<Profession, ProfessionViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ProfessionViewHolder holder, int position, @NonNull Profession model) {
                holder.professionName.setText(model.getName());
            }

            @NonNull
            @Override
            public ProfessionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.rv_profession,
                        viewGroup, false);
                return new ProfessionViewHolder(view);
            }
        };
        deleteItem(mFra);
        mFra.startListening();
        mRvProfessions.setAdapter(mFra);

    }

    private void initListeners() {
        mButtonAddNewProfession.setOnClickListener(view -> addProfession());
    }

    private void addProfession() {
        String profession = mEditProfessionName.getText().toString().trim();
        if (profession.equals("")) {
            mEditProfessionName.setError("Incomplete name.");
            return;
        }

        // Add new profession
        FirebaseDatabase.getInstance().getReference().child("Professions").child(profession.toLowerCase()).
                setValue(new Profession(profession));
        Snackbar.make(Objects.requireNonNull(getView()), "New profession added successfully", Snackbar.LENGTH_LONG);
        mEditProfessionName.setText("");
    }

    private void deleteItem(FirebaseRecyclerAdapter<Profession, ProfessionViewHolder> fra) {
        ItemTouchHelper.SimpleCallback recycleTouchHelper = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                // TODO Add Delete and Undo mode
                Toast.makeText(getContext(), "Profession deleted!", Toast.LENGTH_LONG).show();
                // Deleting event from database
                fra.getRef(viewHolder.getLayoutPosition()).removeValue();

            }
        };

        // Attaching recycler with ItemTouchHelper
        new ItemTouchHelper(recycleTouchHelper).attachToRecyclerView(mRvProfessions);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
