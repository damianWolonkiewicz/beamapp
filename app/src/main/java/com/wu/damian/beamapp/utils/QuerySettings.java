package com.wu.damian.beamapp.utils;

import android.content.Context;
import android.preference.PreferenceManager;

public class QuerySettings {
    private final static String PREF_QUERY_SETTINGS_INDIVIDUAL = "settings_i";
    private final static String PREF_QUERY_SETTINGS_PROFESSION = "settings_p";

    public static boolean getStoredSettingsIndividual(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(PREF_QUERY_SETTINGS_INDIVIDUAL, false);
    }

    public static void setStoredSettingsIndividual(Context context, boolean state) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putBoolean(PREF_QUERY_SETTINGS_INDIVIDUAL, state)
                .apply();
    }

    public static boolean getStoredSettingsProfession(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(PREF_QUERY_SETTINGS_PROFESSION, true);
    }

    public static void setStoredSettingsProfession(Context context, boolean state) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putBoolean(PREF_QUERY_SETTINGS_PROFESSION, state)
                .apply();
    }
}
