package com.wu.damian.beamapp.fragments.admin;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.database.model.Task;
import com.wu.damian.beamapp.database.model.User;
import com.wu.damian.beamapp.utils.ReceiveProfessions;
import com.wu.damian.beamapp.utils.timepicker.MyTimePicker;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddTaskFragment extends BaseFragment {

    @BindViews({R.id.edit_task_name, R.id.edit_description})
    EditText[] editTexts;
    @BindViews({R.id.spinner_profession, R.id.spinner_day_of_week})
    Spinner[] spinners;
    @BindView(R.id.button_push_task)
    Button buttonPush;
    Unbinder unbinder;
    @BindView(R.id.text_hour_from)
    TextView textHourFrom;
    @BindView(R.id.text_hour_to)
    TextView textHourTo;
    @BindView(R.id.toggleButton)
    ToggleButton toggleButton;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_task, container, false);
        unbinder = ButterKnife.bind(this, view);
        initListeners(inflater);
        receiveProfessions(inflater);
        return view;
    }

    private void receiveProfessions(@NonNull LayoutInflater inflater) {
        ReceiveProfessions.receiveProfessions(professions -> {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),
                    android.R.layout.simple_spinner_item, (List<String>) professions);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinners[0].setAdapter(adapter);
        });
    }

    private void receiveUsers(@NonNull LayoutInflater inflater) {
        ReceiveProfessions.receiveUsers(users -> {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),
                    android.R.layout.simple_spinner_item, (List<String>) users);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinners[0].setAdapter(adapter);
        });
    }

    private void onPush() {
        String related = spinners[0].getSelectedItem().toString();
        String day = spinners[1].getSelectedItem().toString();
        String taskName = editTexts[0].getText().toString();
        String description = editTexts[1].getText().toString();
        String thf = textHourFrom.getText().toString();
        String tht = textHourTo.getText().toString();

        if (description.trim().equals("")
                || taskName.trim().equals("") || day.equals("") || related.equals("")) {
            Toast.makeText(getContext(), "Please fill in the empty field(s)",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if (thf.equals(getResources().getString(R.string.hour_from))) {
            Toast.makeText(getContext(), "Please select Start Hour", Toast.LENGTH_SHORT).show();
            return;
        }

        if (tht.equals(getResources().getString(R.string.hour_to))) {
            Toast.makeText(getContext(), "Please select End Hour", Toast.LENGTH_SHORT).show();
            return;
        }

        String hs = thf.substring(0, 2);
        String he = tht.substring(0, 2);
        String ms = thf.substring(5, 7);
        String me = tht.substring(5, 7);
        Log.d(MainActivity.TAG, "onPush: " + thf + " " + tht + " " + hs + " " + he + " " + ms + " " + me);
        if (!(Integer.valueOf(hs) <= Integer.valueOf(he) && Integer.valueOf(ms) < (Integer.valueOf(me)))) {
            Toast.makeText(getActivity(), "Please correct selected hours .", Toast.LENGTH_LONG).show();
            return;
        }

        DatabaseReference tasksReference = FirebaseDatabase.getInstance().getReference()
                .child("Tasks").child(day + related);
        String hash = tasksReference.push().getKey();
        Task task = new Task(thf, tht, taskName, description, hash);
        tasksReference.child(Objects.requireNonNull(hash)).setValue(task);

        String s = "'" + taskName + "'" + "  added." + thf + " - " + tht;
        Snackbar.make(Objects.requireNonNull(getView()), s, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        for (EditText e : editTexts)
            e.setText("");
        textHourFrom.setText(getResources().getText(R.string.hour_from));
        textHourTo.setText(getResources().getText(R.string.hour_to));

    }

    private void initListeners(@NonNull LayoutInflater inflater) {
        buttonPush.setOnClickListener(view -> onPush());
        textHourFrom.setOnClickListener(v -> MyTimePicker.setTimeFromTomePicker(getContext(), textHourFrom));
        textHourTo.setOnClickListener(v -> MyTimePicker.setTimeFromTomePicker(getContext(), textHourTo));
        toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            // Add for individual user
            if (isChecked)
                receiveUsers(inflater);
            else // Add for profession (group of users)
                receiveProfessions(inflater);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(MainActivity.TAG, "onDestroyView: addTaskDestroyed");
        unbinder.unbind();
    }

    public static AddTaskFragment getInstance() {
        return new AddTaskFragment();
    }

    public AddTaskFragment() {
        // Required empty public constructor
    }

}
