package com.wu.damian.beamapp.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.database.interfaces.FirebaseCallback;
import com.wu.damian.beamapp.database.model.User;
import com.wu.damian.beamapp.fragments.dialogs.ForgotPasswordDialog;
import com.wu.damian.beamapp.utils.QueryLogon;
import com.wu.damian.beamapp.utils.UpdateToken;

import java.util.Objects;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment {

    @BindViews({R.id.edit_email, R.id.edit_password})
    EditText[] editTexts;
    @BindView(R.id.check_box_remember_me)
    CheckBox checkBoxRememberMe;
    @BindView(R.id.button_log_in)
    Button btnLogIn;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.text_forgot_password)
    TextView textForgotPassword;
    private Unbinder unbinder;
    private Activity mActivity;
    private Context mContext;
    private DrawerLayout drawer;
    // Current logged user object
    public static User user;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        initVars();
        blockNavigationDrawer();


        // Check if user tap 'Remember me' checkbox in last app use and whether has internet connection.
        // Otherwise will be logged out.
        if (isRememberMe()) {
            progressBarAndOrientationLogic(View.VISIBLE, false);
            logIn();
        } else {
            setRememberMe(false);
            progressBarAndOrientationLogic(View.INVISIBLE, true);
        }
        initListeners();
        return view;
    }


    private void logIn() {
        // Receiving information about current user profession from Firebase Database
        getCurrUserInfo(user -> {
            // Setting info about current user profession
            String userProfession = (String) user;
            if (user != null && !userProfession.isEmpty()) {
                Log.d(MainActivity.TAG, "logIn: " + user);
                progressBarAndOrientationLogic(View.INVISIBLE, false);
                setOrientationChange(true);
                // Update token during user log in.
                UpdateToken.update(FirebaseInstanceId.getInstance().getToken());

                // Move to next fragment
                getNavigation().changeFragment(HomeFragment.getInstance());
            } else {
                // User probably have empty profile fields in database
                Toast.makeText(mActivity,
                        "Sorry! Something goes wrong with log you in. Please try again or contact with administrator.",
                        Toast.LENGTH_LONG).show();
                FirebaseAuth.getInstance().signOut();
                progressBarAndOrientationLogic(View.INVISIBLE, true);
            }
        });
    }

    private void getCurrUserInfo(FirebaseCallback firebaseCallback) {
        // Log.d(MainActivity.TAG, "getCurrUserInfo: called. Current userId : " + MainActivity.mAuth.getCurrentUser().getUid());
        // Receiving user profession from Firebase
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            Log.d(MainActivity.TAG, "getCurrUserInfo: mAuth.getCurrentUser NotNull");
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    // Receiving user from DB
                    user = dataSnapshot.getValue(User.class);
                    firebaseCallback.onCallback(Objects.requireNonNull(user).getProfession());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(MainActivity.TAG, "onCancelled: " + databaseError.getMessage());
                }
            });
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void signIn(String email, String pass) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(mActivity, task -> {
                    if (task.isSuccessful()) {
                        logIn();
                    } else {
                        Snackbar.make(getView(), "Validation failed. Please try again.",
                                Snackbar.LENGTH_LONG)
                                .show();
                        progressBarAndOrientationLogic(View.INVISIBLE, true);
                    }
                });
    }

    private void logInButton() {
        // Disable btnLogIn and show mProgressBar
        progressBarAndOrientationLogic(View.VISIBLE, false);

        // Check if user has Internet connection
        boolean isConnected = hasInternetConnection();
        // Getting EditText notification passed from user
        String email = editTexts[0].getText().toString().trim();
        String pass = editTexts[1].getText().toString().trim();
        if (isConnected) {

            // Not empty email and password field (without whitespaces)
            if (email.isEmpty()) {
                // Check if we can log in with this notification
                setEditError(editTexts[0], "Email required");
                progressBarAndOrientationLogic(View.INVISIBLE, true);
                return;
            }

            if (pass.isEmpty()) {
                setEditError(editTexts[1], "Password required");
                progressBarAndOrientationLogic(View.INVISIBLE, true);
                return;
            }
            signIn(email, pass);
        } else {
            Snackbar.make(Objects.requireNonNull(getView()), "Error! Please, check your internet connection.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            progressBarAndOrientationLogic(View.INVISIBLE, true);
        }
    }

    private void initListeners() {
        // Saving checkBox state (remember last login)
        checkBoxRememberMe.setOnCheckedChangeListener((CompoundButton cb, boolean isChecked) ->
                setRememberMe(cb.isChecked()));
        btnLogIn.setOnClickListener(view -> logInButton());
        textForgotPassword.setOnClickListener(view ->
                new ForgotPasswordDialog().show(getNavigation().getFragmentManagerV4(), "ForgotPassword"));

    }

    // Is user checked "Remember me" checkbox in last login?
    private boolean isRememberMe() {

        return getRememberMe() && hasInternetConnection() && FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    // Method is use for block orientation change during login process.
    private void setOrientationChange(boolean setOrient) {
        if (setOrient)
            mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        else
            mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
    }

    private void progressBarAndOrientationLogic(int visibility, boolean btnOrientEnabled) {
        progressBar.setVisibility(visibility);
        btnLogIn.setEnabled(btnOrientEnabled);
        setOrientationChange(btnOrientEnabled);
    }

    private boolean getRememberMe() {
        return QueryLogon.getStoredState(mContext);
    }

    private void setRememberMe(boolean isRememberMe) {
        QueryLogon.setStoredState(getContext(), isRememberMe);
    }

    // Empty/incorrect login notification
    private void setEditError(EditText et, String e) {
        et.setError(e);
        et.setText("");
        btnLogIn.setEnabled(true);
    }

    private void initVars() {
        mActivity = getActivity();
        mContext = getContext();
        drawer = mActivity.findViewById(R.id.main_drawer_layout);
        // Hide progress bar
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void blockNavigationDrawer() {
        // Block Navigation drawer in LoginFragment
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    private boolean hasInternetConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public LoginFragment() {
        // Required empty public constructor
    }
}
