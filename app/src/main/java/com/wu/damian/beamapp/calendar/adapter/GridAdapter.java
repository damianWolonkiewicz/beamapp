package com.wu.damian.beamapp.calendar.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wu.damian.beamapp.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class GridAdapter extends ArrayAdapter {
    private LayoutInflater mInflater;
    private List<Date> mMonthlyDates;
    private Calendar mCurrentDate;
    private List<Date> mAllEvents;


    public GridAdapter(Context context, List<Date> mMonthlyDates, Calendar mCurrentDate, List<Date> mAllEvents) {
        super(context, R.layout.single_cell_layout);
        this.mMonthlyDates = mMonthlyDates;
        this.mCurrentDate = mCurrentDate;
        this.mAllEvents = mAllEvents;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // x date from generated calendar
        Date mDate = mMonthlyDates.get(position);

        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);

        //  Getting info about every cell in calendar
        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        // Receiving info about current year and month.
        int currentMonth = mCurrentDate.get(Calendar.MONTH) + 1;
        int currentYear = mCurrentDate.get(Calendar.YEAR);
        View mView = convertView;

        if (mView == null) {
            mView = mInflater.inflate(R.layout.single_cell_layout, parent, false);
        }

        TextView cellNumber = mView.findViewById(R.id.calendar_date_id);
        // Setting day number "gray" text color, which doesn't belong to current month
        if (!(displayMonth == currentMonth && displayYear == currentYear))
            cellNumber.setTextColor(parent.getResources().getColor(R.color.colorCalendarOtherMonthDay));
        // Setting number of the day in TextView
        cellNumber.setText(String.valueOf(dayValue));
        // Add events to the calendar
        View eventDot = mView.findViewById(R.id.event_dot);

        StringBuilder day1 = new StringBuilder();
        day1.append(dayValue).append(displayMonth).append(displayYear);
        for (Date d : mAllEvents) {
            dateCal.setTime(d);
            String day2 = String.valueOf(dateCal.get(Calendar.DAY_OF_MONTH))
                    + String.valueOf(dateCal.get(Calendar.MONTH) + 1)
                    + String.valueOf(dateCal.get(Calendar.YEAR));
            // If event exits in checking day is marking by red dot
            if (day2.equals(day1.toString()))
                eventDot.setBackground(getContext().getResources().getDrawable(R.drawable.calendar_event_dot));
        }

        return mView;
    }

    @Override
    public int getCount() {
        return mMonthlyDates.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return mMonthlyDates.get(position);
    }

    @Override
    public int getPosition(Object item) {
        return mMonthlyDates.indexOf(item);
    }

}