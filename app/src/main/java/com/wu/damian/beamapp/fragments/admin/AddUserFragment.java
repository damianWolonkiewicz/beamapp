package com.wu.damian.beamapp.fragments.admin;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.database.model.User;
import com.wu.damian.beamapp.fragments.LoginFragment;
import com.wu.damian.beamapp.utils.ReceiveProfessions;
import com.wu.damian.beamapp.utils.UpdateToken;

import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddUserFragment extends BaseFragment {

    @BindView(R.id.edit_user_name)
    EditText mEditRegUserName;
    @BindView(R.id.edit_reg_email)
    EditText mEditRegEmail;
    @BindView(R.id.spinner_profession)
    Spinner mSpinnerProfession;
    @BindView(R.id.button_reg_confirm)
    Button mButtonRegConfirm;
    @BindView(R.id.edit_reg_password)
    EditText mEditRegPassword;
    Unbinder mUnbinder;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_user, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        initListeners();
        receiveProfessions(inflater);
        return view;
    }

    private void receiveProfessions(@NonNull LayoutInflater inflater) {
        ReceiveProfessions.receiveProfessions(professions -> {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),
                    android.R.layout.simple_spinner_item, (List<String>) professions);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerProfession.setAdapter(adapter);
        });
    }


    private void initListeners() {
        mButtonRegConfirm.setOnClickListener(v -> {
            progressBarLogic(View.VISIBLE, false);
            String email = mEditRegEmail.getText().toString().trim();
            String user = mEditRegUserName.getText().toString().trim();
            String pass = mEditRegPassword.getText().toString().trim();
            Pattern emailPattern = Pattern.compile(".+@[a-z0-9]+\\.[a-z]{2,4}");
            Log.d(MainActivity.TAG, "initListeners: emailRegEx : " + emailPattern.matcher(email).matches());

            if (user.isEmpty()) {
                mEditRegUserName.setError("User Name Required");
                progressBarLogic(View.INVISIBLE, true);
                return;
            }

            if (email.isEmpty() || !emailPattern.matcher(email).matches()) {
                mEditRegEmail.setError("Email required");
                progressBarLogic(View.INVISIBLE, true);
                return;
            }

            if (pass.isEmpty()) {
                mEditRegPassword.setError("Password Required");
                progressBarLogic(View.INVISIBLE, true);
                return;
            }
            if (pass.length() < 6) {
                mEditRegPassword.setError("Password minimum length is 6 characters");
                progressBarLogic(View.INVISIBLE, true);
                return;
            }
            createNewUser(mEditRegEmail.getText().toString(), mEditRegPassword.getText().toString(), user);
        });
    }

    private void createNewUser(String email, String p, String userName) {

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, p)
                .addOnCompleteListener(getActivity(), task -> {

                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        updateUI(email, userName);
                        clearEditTexts();
                        updateFirebase(userName, email, mSpinnerProfession.getSelectedItem().toString(),
                                FirebaseAuth.getInstance().getCurrentUser().getUid());
                        Toast.makeText(getContext(), "User created successfully. You are current logged in as "
                                + email, Toast.LENGTH_LONG).show();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(MainActivity.TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(getActivity(), "Authentication failed. Please try again.",
                                Toast.LENGTH_SHORT).show();
                        progressBarLogic(View.INVISIBLE, true);
                    }
                });
    }


    private void updateFirebase(String name, String email, String profession, String uid) {
        User user = new User(name, email, profession, uid);
        LoginFragment.user = user;
        Log.d(MainActivity.TAG, "updateFirebase: user info : " + user.getName() + " " + user.getEmail());
        // Update Firebase Users node
        FirebaseDatabase.getInstance().getReference().child("Users").child(uid).setValue(user);
        // Generate token
        UpdateToken.update(FirebaseInstanceId.getInstance().getToken());
        progressBarLogic(View.INVISIBLE, true);
    }


    private void updateUI(String e, String userName) {
        NavigationView nav = getActivity().findViewById(R.id.nav_list);
        nav.getMenu().clear();
        // Getting profession from spinner
        if (LoginFragment.user.getProfession().equals("Admin") || LoginFragment.user.getProfession().equals("Manager"))
            nav.inflateMenu(R.menu.drawer_menu_admin);
        else nav.inflateMenu(R.menu.drawer_menu);
        TextView un = getActivity().findViewById(R.id.nav_user_name);
        TextView uem = getActivity().findViewById(R.id.nav_user_email);
        un.setText(userName);
        uem.setText(e);
    }


    private void clearEditTexts() {
        mEditRegPassword.setText("");
        mEditRegEmail.setText("");
        mEditRegUserName.setText("");
    }

    private void progressBarLogic(int visible, boolean enabled) {
        mProgressBar.setVisibility(visible);
        mButtonRegConfirm.setEnabled(enabled);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    public AddUserFragment() {
        // Required empty public constructor
    }
}
