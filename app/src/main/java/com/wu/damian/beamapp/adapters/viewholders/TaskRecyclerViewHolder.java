package com.wu.damian.beamapp.adapters.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wu.damian.beamapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskRecyclerViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.text_task_name)
    public TextView textTaskName;
    @BindView(R.id.text_start_hour)
    public TextView textStartHour;
    @BindView(R.id.text_end_hour)
    public TextView textEndHour;
    @BindView(R.id.text_task_desc)
    public TextView textTaskDesc;
    @BindView(R.id.checkBox_task_done)
    public CheckBox checkBoxTaskDone;


    public TaskRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
