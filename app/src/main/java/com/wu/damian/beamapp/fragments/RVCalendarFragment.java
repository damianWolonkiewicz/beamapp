package com.wu.damian.beamapp.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.adapters.viewholders.EventViewHolder;
import com.wu.damian.beamapp.calendar.CalendarCustomView;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.database.model.Event;
import com.wu.damian.beamapp.database.model.User;
import com.wu.damian.beamapp.utils.BundleKey;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class RVCalendarFragment extends BaseFragment {

    @BindView(R.id.event_recycler_view)
    RecyclerView eventRecyclerView;
    private CalendarCustomView calendarView;
    private Unbinder unbinder;
    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<Event, EventViewHolder> firebaseRecyclerAdapter;
    private String monthYear;
    private String day;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        monthYear = bundle != null ? bundle.getString(BundleKey.Info.monthYear) : null;
        day = bundle != null ? bundle.getString(BundleKey.Info.day) : null;
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rv_calendar, container, false);
        unbinder = ButterKnife.bind(this, view);
        initVariables();
        initRecycler();
        return view;
    }


    private void initVariables() {
        // Set Firebase reference to suitable path
        databaseReference = FirebaseDatabase.getInstance().getReference("Events/" + monthYear + "/" + day);
        calendarView = getActivity().findViewById(R.id.custom_calendar);
    }

    private void deleteRecycleItem() {
        ItemTouchHelper.SimpleCallback recycleTouchHelper = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                // TODO Add Delete and Undo mode
                Toast.makeText(getContext(), "Event deleted!", Toast.LENGTH_LONG).show();
                // Deleting event from database
                firebaseRecyclerAdapter.getRef(viewHolder.getLayoutPosition()).removeValue();
                refreshCalendar();
            }
        };
        // Attaching eventRecycler with ItemTouchHelper
        new ItemTouchHelper(recycleTouchHelper).attachToRecyclerView(eventRecyclerView);
    }

    private void refreshCalendar() {
        calendarView.setUpCalendarAdapter();
    }

    private void initRecycler() {
        // Set recyclerView layout
        eventRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        // Get notification from Firebase (in RecyclerView option)
        FirebaseRecyclerOptions<Event> options = new FirebaseRecyclerOptions.Builder<Event>()
                .setQuery(databaseReference, Event.class).build();
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Event, EventViewHolder>(options) {
            @Override
            // Method set notification in each ViewHolder object (in each RecyclerView subview)
            protected void onBindViewHolder(@NonNull EventViewHolder holder, int position, @NonNull Event model) {
                holder.itemView.findViewById(R.id.text_involved);
                holder.setPath(getRef(position));
                setTextValues(holder, model);
            }

            @NonNull
            @Override
            public EventViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.rv_event,
                        viewGroup, false);
                return new EventViewHolder(view);
            }
        };
        firebaseRecyclerAdapter.startListening();
        eventRecyclerView.setAdapter(firebaseRecyclerAdapter);
        // Only admin/manager is able to remove events
        if (LoginFragment.user.getProfession().equals("Admin") ||
                LoginFragment.user.getProfession().equals("Manager"))
            deleteRecycleItem();
    }

    // Change TextView color if user is connected with event
    private void setInvolvedColor(TextView tv) {
        tv.setTextColor(getResources().getColor(R.color.colorInvolved));
    }

    private void setTextValues(EventViewHolder holder, Event model) {
        holder.eventName.setText(model.getEventType());
        holder.eventMessage.setText(model.getMessage());

        StringBuilder sb = new StringBuilder();
        for (User u : model.getUsersList()) {
            sb.append(u.getName()).append("\n");
            if (u.getName().equals(LoginFragment.user.getName()))
                setInvolvedColor(holder.itemView.findViewById(R.id.text_involved));
        }
        sb.setLength(sb.length() - 1);
        holder.eventUsers.setText(sb);

    }

    public RVCalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
