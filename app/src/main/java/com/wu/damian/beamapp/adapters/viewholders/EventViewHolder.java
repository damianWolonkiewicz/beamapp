package com.wu.damian.beamapp.adapters.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.wu.damian.beamapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_eventName)
    public TextView eventName;
    @BindView(R.id.text_eventMessage)
    public TextView eventMessage;
    @BindView(R.id.text_eventUsers)
    public TextView eventUsers;
    private DatabaseReference path;

    public EventViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public DatabaseReference getPath() {
        return path;
    }

    public void setPath(DatabaseReference path) {
        this.path = path;
    }
}
