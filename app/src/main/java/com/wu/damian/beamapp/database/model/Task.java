package com.wu.damian.beamapp.database.model;

public class Task {
    private String hourFrom;
    private String hourTo;
    private String taskName;
    private String description;
    private String hash;
    private boolean taskDone;

    public Task() {
    }

    public Task(String hourFrom, String hourTo, String taskName, String description, String hash) {
        this.hourFrom = hourFrom;
        this.hourTo = hourTo;
        this.taskName = taskName;
        this.description = description;
        this.hash = hash;
        taskDone = false;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public boolean getTaskDone() {
        return taskDone;
    }

    public void setTaskDone(boolean taskDone) {
        this.taskDone = taskDone;
    }

    public String getHourFrom() {
        return hourFrom;
    }

    public void setHourFrom(String hourFrom) {
        this.hourFrom = hourFrom;
    }

    public String getHourTo() {
        return hourTo;
    }

    public void setHourTo(String hourTo) {
        this.hourTo = hourTo;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
