package com.wu.damian.beamapp.utils.viewmodel;

import android.arch.lifecycle.ViewModel;

public class AddTaskViewModel extends ViewModel {

    private String hourFrom;
    private String hourTo;

    public String getHourFrom() {
        return hourFrom;
    }

    public void setHourFrom(String hourFrom) {
        this.hourFrom = hourFrom;
    }

    public String getHourTo() {
        return hourTo;
    }

    public void setHourTo(String hourTo) {
        this.hourTo = hourTo;
    }
}
