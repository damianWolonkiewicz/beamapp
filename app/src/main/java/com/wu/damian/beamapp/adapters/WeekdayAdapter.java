package com.wu.damian.beamapp.adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.fragments.NavTimetableFragment;
import com.wu.damian.beamapp.navigation.NavigationInterface;
import com.wu.damian.beamapp.utils.BundleKey;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeekdayAdapter extends RecyclerView.Adapter<WeekdayAdapter.ViewHolder> {

//    private String userProfession;
    private List<String> data = new ArrayList<>();
    private NavigationInterface navInterface;
    private ColorGenerator generator = ColorGenerator.DEFAULT;

    public void setNavigationInterface(NavigationInterface navigationInterface) {
        this.navInterface = navigationInterface;
    }

    public void setData(List<String> data) {
        if (data == null) data = new ArrayList<>();
        this.data = data;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_avatar)
        ImageView letterImage;
        @BindView(R.id.text_view_day_name)
        TextView dayName;
        @BindView(R.id.item_layout_container)
        RelativeLayout itemLayoutContainer;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater
                .from(viewGroup.getContext()).inflate(R.layout.rv_day_week,
                        viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.dayName.setText(data.get(i));
        int color = generator.getRandomColor();
        TextDrawable td = TextDrawable.builder().buildRoundRect(data.get(i).substring(0, 1).toUpperCase(), color, 20);
        viewHolder.letterImage.setImageDrawable(td);

        // Single mView Listener
        viewHolder.itemView.setOnClickListener(view -> {
            if (navInterface != null) {
                Bundle bundle = new Bundle();
                bundle.putString(BundleKey.Info.dayName, data.get(i));
                NavTimetableFragment ntf = NavTimetableFragment.getInstance(bundle);
                navInterface.changeFragment(ntf);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

}
