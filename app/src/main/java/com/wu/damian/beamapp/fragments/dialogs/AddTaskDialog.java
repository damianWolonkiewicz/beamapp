package com.wu.damian.beamapp.fragments.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.database.model.Task;
import com.wu.damian.beamapp.utils.ReceiveProfessions;
import com.wu.damian.beamapp.utils.timepicker.MyTimePicker;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.wu.damian.beamapp.MainActivity.TAG;

public class AddTaskDialog extends DialogFragment {

    @BindView(R.id.text_hour_from)
    TextView textHourFrom;
    @BindView(R.id.text_hour_to)
    TextView textHourTo;
    @BindView(R.id.button_push_task)
    Button button;
    @BindView(R.id.edit_task_name)
    EditText editTaskName;
    @BindView(R.id.edit_description)
    EditText editDescription;
    @BindView(R.id.spinner_profession)
    Spinner spinnerProfession;
    @BindView(R.id.spinner_day_of_week)
    Spinner spinnerDayOfWeek;
    Unbinder unbinder;
    @BindView(R.id.toggleButton)
    ToggleButton toggleButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_task, container, false);
        unbinder = ButterKnife.bind(this, view);
        initListeners(inflater);
        receiveProfessions(inflater);
        return view;
    }

    private void initListeners(@NonNull LayoutInflater inflater) {
        button.setOnClickListener(view -> onPush());
        textHourFrom.setOnClickListener(v -> MyTimePicker.setTimeFromTomePicker(getContext(), textHourFrom));
        textHourTo.setOnClickListener(v -> MyTimePicker.setTimeFromTomePicker(getContext(), textHourTo));
        toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            // Add for individual user
            if (isChecked)
                receiveUsers(inflater);
            else // Add for profession (group of users)
                receiveProfessions(inflater);
        });
    }


    private void receiveProfessions(@NonNull LayoutInflater inflater) {
        ReceiveProfessions.receiveProfessions(professions -> {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),
                    android.R.layout.simple_spinner_item, (List<String>) professions);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinnerProfession.setAdapter(adapter);
        });
    }

    private void receiveUsers(@NonNull LayoutInflater inflater) {
        ReceiveProfessions.receiveUsers(users -> {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),
                    android.R.layout.simple_spinner_item, (List<String>) users);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinnerProfession.setAdapter(adapter);
        });
    }

    private void onPush() {

        String profession = spinnerProfession.getSelectedItem().toString();
        String day = spinnerDayOfWeek.getSelectedItem().toString();
        String taskName = editTaskName.getText().toString();
        String description = editDescription.getText().toString();
        String thf = textHourFrom.getText().toString();
        String tht = textHourTo.getText().toString();

        // Strings trimmed
        if (description.trim().isEmpty()
                || taskName.trim().isEmpty()
                || day.isEmpty() || profession.isEmpty()) {
            toast("Please fill in the empty field(s)");
            return;
        }

        if (thf.equals(getResources().getString(R.string.hour_from))) {
            toast("Please select Start Hour");
            return;
        }
        Log.d(TAG, "onPush: tht: " + tht + " resource: " + getResources().getString(R.string.hour_to));
        if (tht.equals(getResources().getString(R.string.hour_to))) {
            toast("Please select End Hour");
            return;
        }

        String hs = thf.substring(0, 2);
        String he = tht.substring(0, 2);
        String ms = thf.substring(5, 7);
        String me = tht.substring(5, 7);
        Log.d(TAG, "onPush: " + thf + " " + tht + " || " + hs + " " + he + " " + ms + " " + me);

        if (!(Integer.valueOf(hs) <= (Integer.valueOf(he)))) {
            toast("Please correct selected hours");
            return;
        }
        if (Integer.valueOf(hs).equals(Integer.valueOf(he)) && !(Integer.valueOf(ms) < (Integer.valueOf(me)))) {
            toast("Please correct selected hours");
            return;
        }

        DatabaseReference tasksReference = FirebaseDatabase.getInstance().getReference()
                .child("Tasks").child(day + profession);
        String hash = tasksReference.push().getKey();
        Task task = new Task(thf, tht, taskName, description, hash);
        tasksReference.child(Objects.requireNonNull(hash)).setValue(task);

        String s = "'" + taskName + "'" + "  added.  " + thf + " - " + tht;
        toast(s);

        textHourFrom.setText(getResources().getText(R.string.hour_from));
        textHourTo.setText(getResources().getText(R.string.hour_to));
        this.dismiss();
    }

    private void toast(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.MyAnimation_Window;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
