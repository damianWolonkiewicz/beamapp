package com.wu.damian.beamapp.utils;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.TextView;

import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.fragments.LoginFragment;

/**
 * Class inflate NavigationDrawer with content
 */
public class NavDrawerContent {

    public static void setNavDrawerContent(Activity activity) {
        NavigationView nav = activity.findViewById(R.id.nav_list);
        nav.getMenu().clear();
        if (LoginFragment.user != null) {
            if (LoginFragment.user.getProfession().equals("Admin") || LoginFragment.user.getProfession().equals("Manager"))
                nav.inflateMenu(R.menu.drawer_menu_admin);
            else nav.inflateMenu(R.menu.drawer_menu);
            setUserInfo(nav);
        }
    }

    private static void setUserInfo(NavigationView navView) {
        View header = navView.getHeaderView(0);
        TextView userName = header.findViewById(R.id.nav_user_name);
        TextView userEmail = header.findViewById(R.id.nav_user_email);
        userName.setText(LoginFragment.user.getName());
        userEmail.setText(LoginFragment.user.getEmail());
    }
}
