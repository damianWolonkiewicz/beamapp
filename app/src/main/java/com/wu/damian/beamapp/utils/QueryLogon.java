package com.wu.damian.beamapp.utils;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Class is used for storing info about last login state. (Shared preferences)
 */
public class QueryLogon {
    private final static String PREF_QUERY_LOG = "logOn";

    public static boolean getStoredState(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(PREF_QUERY_LOG, false);
    }

    public static void setStoredState(Context context, boolean state) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putBoolean(PREF_QUERY_LOG, state)
                .apply();
    }
}
