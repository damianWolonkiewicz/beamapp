package com.wu.damian.beamapp.utils.timepicker;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TextView;

import java.util.Calendar;

import static java.lang.String.format;

/**
 * Class responsible for generate TimePicker object.
 */
public class MyTimePicker {
    public static void setTimeFromTomePicker(Context context, TextView textText) {
        Calendar c = Calendar.getInstance();
        int h = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        TimePickerDialog dpd = new TimePickerDialog(context, (view, hourOfDay, minute) -> {
            @SuppressLint("DefaultLocale") String s = format("%02d : %02d", hourOfDay, minute);
            textText.setText(s);
        }, h, min, true);
        dpd.show();
    }
}
