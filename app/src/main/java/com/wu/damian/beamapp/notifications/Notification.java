package com.wu.damian.beamapp.notifications;

public class Notification {
    private int icon;
    private String body;
    private String title;
    private String sound;

    public Notification(int icon, String body, String title) {
        this.icon = icon;
        this.body = body;
        this.title = title;
        sound = "default";
    }

    public Notification() {
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
