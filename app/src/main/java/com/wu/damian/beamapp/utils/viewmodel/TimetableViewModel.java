package com.wu.damian.beamapp.utils.viewmodel;

import android.arch.lifecycle.ViewModel;

/**
 * Class is responsible for store day, profession - content -  while activity, fragment etc. is destroyed.
 */
public class TimetableViewModel extends ViewModel {

    private String day;
    private String profession;

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
