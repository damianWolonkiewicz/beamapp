package com.wu.damian.beamapp.database.interfaces;

/**
 * The interface can be used for receiving notification from an asynchronous Firebase task.
 * Purpose of this interface is ensure receive notification from Firebase Database before app make next step.
 */
public interface FirebaseCallback {
    void onCallback(Object data);
}
