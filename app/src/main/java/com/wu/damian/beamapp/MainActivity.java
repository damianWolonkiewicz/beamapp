package com.wu.damian.beamapp;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.fragments.CalendarFragment;
import com.wu.damian.beamapp.fragments.HomeFragment;
import com.wu.damian.beamapp.fragments.LoginFragment;
import com.wu.damian.beamapp.fragments.SettingsFragment;
import com.wu.damian.beamapp.fragments.TimetableFragment;
import com.wu.damian.beamapp.fragments.admin.AddTaskFragment;
import com.wu.damian.beamapp.fragments.admin.AddUserFragment;
import com.wu.damian.beamapp.fragments.admin.ManageProfessionFragment;
import com.wu.damian.beamapp.fragments.dialogs.AboutBeamAppDialog;
import com.wu.damian.beamapp.navigation.NavigationInterface;
import com.wu.damian.beamapp.utils.BundleKey;
import com.wu.damian.beamapp.utils.NavDrawerContent;
import com.wu.damian.beamapp.utils.QueryLogon;
import com.wu.damian.beamapp.utils.viewmodel.UserViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationInterface, NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.main_drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_list)
    NavigationView navList;
    private FragmentManager fManager;
    // Position of drawer list item
    private int currentPos = 0;
    // Stored info about current logged in user
    private UserViewModel uvm;
    public static final String TAG = "mINFO";
    // Notification channel
    public static final String NOTIFICATION_CHANNEL_ID = "beam_app";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Setting standard AppTheme instead of StartAppTheme (start launching screen)
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        uvm = ViewModelProviders.of(this).get(UserViewModel.class);
        ButterKnife.bind(this);
        initVariables();
        setNavigationView();
        initListeners();

        Log.d(TAG, "onCreate: userNULL??" + LoginFragment.user+ " uvm,getUser() " + uvm.getUser());

        if (savedInstanceState == null) {
            fManager.beginTransaction().replace(R.id.fragment_container, LoginFragment.newInstance(),
                    BundleKey.Info.drawableItemPosition).commit();
            Log.d(TAG, "onCreate: BundleKey.Info.drawableItemPosition : " + BundleKey.Info.drawableItemPosition);
        }
        else {
            if (FirebaseAuth.getInstance().getCurrentUser() == null)
                logOut();
            currentPos = savedInstanceState.getInt(BundleKey.Info.drawableItemPosition);
            Log.d(TAG, "onCreate: " + currentPos );
            if (uvm.getUser() != null) LoginFragment.user = uvm.getUser();
            if (currentPos == R.id.nav_timetable)
                changeFragment(HomeFragment.getInstance(), false, true);
            NavDrawerContent.setNavDrawerContent(this);
        }
    }

    // Select item clicked in navigation drawer
    private void selectItem(int pos) {
        currentPos = pos;
        switch (pos) {
            case R.id.nav_calendar:
                changeFragment(CalendarFragment.getInstance());
                break;
            case R.id.nav_timetable:
                changeFragment(HomeFragment.getInstance());
                break;
            case R.id.nav_add_user:
                changeFragment(new AddUserFragment());
                break;
            case R.id.nav_add_task:
                changeFragment(new AddTaskFragment());
                break;
            case R.id.nav_add_profession:
                changeFragment(new ManageProfessionFragment());
                break;
            case 111:
                changeFragment(new TimetableFragment(), false, true);
                break;
            case R.id.nav_log_out:
                logOut();
                break;
            case R.id.nav_about_app:
                new AboutBeamAppDialog().show(getFragmentManagerV4(), "AboutApp");
                break;
            default:
                logOut();
        }
        // Close navigation drawer
        closeDrawer();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(BundleKey.Info.drawableItemPosition, currentPos);
        Log.d(TAG, "onSaveInstanceState: " + currentPos);
        if (LoginFragment.user != null) {
            uvm.setUser(LoginFragment.user);
            Log.d(TAG, "onSaveInstanceState: called ! user = " + uvm.getUser().getProfession());
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        selectItem(menuItem.getItemId());
        return true;
    }

    private void setNavigationView() {
        navList.setNavigationItemSelectedListener(this);
    }

    private void initVariables() {
        fManager = getSupportFragmentManager();
    }

    private void initListeners() {
        fManager.addOnBackStackChangedListener(() -> {
            // Refresh token when user is logged in
//            if (mAuth.getCurrentUser() != null)
//                // Refresh token - able to receive notification
//                updateToken(FirebaseInstanceId.getInstance().getToken());

            // Only two of all item from navigation drawer can be checked
            Fragment fragment = fManager.findFragmentById(R.id.fragment_container);

            if (fragment instanceof CalendarFragment) {
                navList.getMenu().findItem(R.id.nav_calendar).setChecked(true);
                currentPos = R.id.nav_calendar;
                return;
            }

            if (fragment instanceof TimetableFragment) {
                navList.getMenu().findItem(R.id.nav_timetable).setChecked(true);
                currentPos = R.id.nav_timetable;
                return;
            }

            if (fragment instanceof HomeFragment) {
                navList.getMenu().findItem(R.id.nav_timetable).setChecked(true);
                currentPos = R.id.nav_timetable;
                return;
            }

            if (fragment instanceof LoginFragment) {
                currentPos = R.id.nav_log_out;
            }

            if (fragment instanceof AddUserFragment) {
                currentPos = R.id.nav_add_user;
            }

            if (fragment instanceof AddTaskFragment) {
                currentPos = R.id.nav_add_task;
            }

            if (fragment instanceof ManageProfessionFragment) {
                currentPos = R.id.nav_add_profession;
            }

            // Uncheck item from navigation drawer
            for (int i = 0; i < navList.getMenu().size(); i++)
                navList.getMenu().getItem(i).setChecked(false);

        });
    }

    private void navigateTo(Fragment fragment, boolean addToBackStack, boolean withAnimation) {
        if (fManager == null || fragment == null) return;
        FragmentTransaction transaction = fManager.beginTransaction();
        if (withAnimation)
            transaction.setCustomAnimations(android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        if (addToBackStack) transaction.addToBackStack(fragment.toString());
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commitAllowingStateLoss();
    }

    private void logOut() {
        QueryLogon.setStoredState(this, false);
        // Clear fragment back stack
        FragmentManager fm = fManager;
        for (int i = 0; i < fm.getBackStackEntryCount(); i++)
            fm.popBackStack();
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    public FragmentManager getFragmentManagerV4() {
        return fManager;
    }

    @Override
    public void changeFragment(BaseFragment fragment) {
        navigateTo(fragment, true, true);
    }

    @Override
    public void changeFragment(BaseFragment fragment, boolean addToBackStack) {
        navigateTo(fragment, addToBackStack, true);
    }

    @Override
    public void changeFragment(BaseFragment fragment, boolean addToBackStack,
                               boolean withAnimation) {
        navigateTo(fragment, addToBackStack, withAnimation);
    }

    private void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            /* Check if current fragment is HomeFragment, if is,  onKeyDown() is disabled  */
            Fragment myFrag = fManager.findFragmentById(R.id.fragment_container);
            if (myFrag instanceof HomeFragment) {
                Log.d(TAG, "onKeyDown: downKey ignored");
                return false;
            }
            Log.d(TAG, "onKeyDown: downKey working");
            return super.onKeyDown(keyCode, event);
        }
        return true;
    }


}
