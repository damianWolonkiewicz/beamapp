package com.wu.damian.beamapp.notifications.service;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wu.damian.beamapp.utils.NotificationHelper;

import java.util.Map;

public class FirebaseInstanceService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getNotification() != null)
            NotificationHelper.showNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), this);
        else
            showNotification(remoteMessage.getData());
    }

    private void showNotification(Map<String, String> data) {
        NotificationHelper.showNotification(data.get("title"), data.get("body"), this);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }
}
