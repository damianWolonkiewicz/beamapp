package com.wu.damian.beamapp.database.model;

import java.util.List;

public class Event {
    private String message;
    private String eventType;
    private List<User> usersList;

    /** Default constructor is REQUIRED to receiving notification from database. */
    public Event() {
    }

    public Event(String message, String eventType, List<User> usersList) {
        this.message = message;
        this.eventType = eventType;
        this.usersList = usersList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }
}
