package com.wu.damian.beamapp.notifications;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAA4K2Ja0g:APA91bHxiNMgK0yBjrJlfsYktZZjEInP5smaZRWS8tj8uUlNspzVRtslJUxPnztNSpR6hkY7BJT81wciqN_ORg_DxHIl8QevkTAbWyl7rphjdPjk7f5AIVzxgwgUZkHgouFcyuu383q4"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);

}
