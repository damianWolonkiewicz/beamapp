package com.wu.damian.beamapp.utils;

public class BundleKey {
    public static class Info {
        public static final String profession = "profession";
        public static final String dayName = "dayName";

        //  RVCalendarFragment
        public static final String monthYear = "monthYear";
        public static final String day = "day";

        public static final String drawableItemPosition = "drawableItemPosition";

        public static final String navDestination = "navDestination";

    }

}
