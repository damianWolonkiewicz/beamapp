package com.wu.damian.beamapp.calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.calendar.adapter.GridAdapter;
import com.wu.damian.beamapp.database.interfaces.FirebaseCallback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.wu.damian.beamapp.MainActivity.TAG;

public class CalendarCustomView extends LinearLayout {

    //  Max number of day cells
    private static final int MAX_CALENDAR_COLUMN = 42;
    private Context mContext;
    private ImageView mPreviousButton, mNextButton;
    private TextView mCurrentDate;
    // Calendar gird mView
    public GridView mCalendarGridView;
    public Calendar mCal = Calendar.getInstance(Locale.ENGLISH);
    public GridAdapter mAdapter;


    @SuppressLint("DefaultLocale")
    public void setUpCalendarAdapter() {
        // Date object in cell !
        List<Date> dayValueInCells = new ArrayList<>();

        Calendar calendar = (Calendar) mCal.clone();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = (calendar.get(Calendar.DAY_OF_WEEK)) - 2;
        calendar.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);

        // Fill in GridView mCells with specific Date objects
        while (dayValueInCells.size() < MAX_CALENDAR_COLUMN) {
            dayValueInCells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        // Implementing FirebaseCallback onCallback() method,
        // Method's body is running after receive notification from Firebase
        readDataFromFirebase(ed -> {
            List<Date> eventDays = (List<Date>) ed;
            // Setting current date
            String sDate = mCal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + ",  " + mCal.get(Calendar.YEAR);
            mCurrentDate.setText(sDate);
            mAdapter = new GridAdapter(mContext, dayValueInCells, mCal, eventDays);
            mCalendarGridView.setAdapter(mAdapter);
        });
    }


    @SuppressLint("DefaultLocale")
    private void readDataFromFirebase(FirebaseCallback firebaseCallback) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        List<Date> eventDays = new ArrayList<>();
        String sb = String.format("%02d", mCal.get(Calendar.MONTH) + 1) + mCal.get(Calendar.YEAR);

        // Firebase Connection - receiving required reference
        DatabaseReference dr = FirebaseDatabase.getInstance().getReference().child("Events").child(sb);
        dr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    @SuppressLint("DefaultLocale") String strDate = ds.getKey() + "-" + String.format("%02d", mCal.get(Calendar.MONTH) + 1)
                            + "-" + String.format("%02d", mCal.get(Calendar.YEAR));
                    Date date = null;
                    try {
                        date = sdf.parse(strDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    eventDays.add(date);
                }
                firebaseCallback.onCallback(eventDays);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
    }

    public CalendarCustomView(Context mContext, AttributeSet attrs) {
        super(mContext, attrs);
        this.mContext = mContext;
        initializeUI();
        setUpCalendarAdapter();
        initListeners();
    }


    private void initializeUI() {
        // TODO async task inflater ?
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.calendar_layout, this);
        mPreviousButton = view.findViewById(R.id.previous_month);
        mNextButton = view.findViewById(R.id.next_month);
        mCurrentDate = view.findViewById(R.id.display_current_date);
        mCalendarGridView = view.findViewById(R.id.calendar_grid);
    }

    // Previous button listener
    private void setPreviousButtonClickEvent() {
        mPreviousButton.setOnClickListener(v -> {
            mCal.add(Calendar.MONTH, -1);
            setUpCalendarAdapter();
        });
    }

    // Next button listener
    private void setNextButtonClickEvent() {
        mNextButton.setOnClickListener(v -> {
            mCal.add(Calendar.MONTH, 1);
            setUpCalendarAdapter();
        });
    }

    private void initListeners() {
        setPreviousButtonClickEvent();
        setNextButtonClickEvent();
    }

}
