package com.wu.damian.beamapp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.fragments.CalendarFragment;
import com.wu.damian.beamapp.utils.StateVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Prepare notification for 'Employee choose' Spinner
 */
public class EmployeeAdapter extends ArrayAdapter<StateVO> {
    private Context mContext;
    public static ArrayList<StateVO> listState;
    private boolean isFromView = false;

    public EmployeeAdapter(Context context, int resource, List<StateVO> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<StateVO>) objects;
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.user_spinner_item, null);
            holder = new ViewHolder();
            holder.mTextView = convertView
                    .findViewById(R.id.text_checkbox_user);
            holder.mCheckBox = convertView
                    .findViewById(R.id.checkbox_user);
            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        holder.mTextView.setText(listState.get(position).getTitle());

        // To check whether checked event fire from getView() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).isSelected());
        isFromView = false;

        if (position == 0) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
            holder.mTextView.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
//            holder.mTextView.setTextSize(18);
        } else holder.mCheckBox.setVisibility(View.VISIBLE);
        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!isFromView) listState.get(position).setSelected(isChecked);
        });
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}