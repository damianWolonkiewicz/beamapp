package com.wu.damian.beamapp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wu.damian.beamapp.R;

import java.util.ArrayList;
import java.util.List;

public class EventTypeAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private List<String> eventsTypeList;


    public EventTypeAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        mContext = context;
        eventsTypeList = objects;
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        convertView = layoutInflater.inflate(R.layout.event_spinner_item, null);
        holder = new ViewHolder();
        holder.textEventType = convertView.findViewById(R.id.event_spinner_item);
        convertView.setTag(holder);

        // Set event name
        holder.textEventType.setText(eventsTypeList.get(position));
        // Set font type bold for default record
        if (position == 0) {
            holder.textEventType.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
            holder.textEventType.setTextSize(18);
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private class ViewHolder {
        private TextView textEventType;
    }

}
