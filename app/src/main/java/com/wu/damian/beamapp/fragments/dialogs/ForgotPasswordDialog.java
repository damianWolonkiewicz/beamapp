package com.wu.damian.beamapp.fragments.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.wu.damian.beamapp.R;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.wu.damian.beamapp.MainActivity.TAG;

public class ForgotPasswordDialog extends DialogFragment {


    Unbinder unbinder;
    @BindView(R.id.button_send_reset)
    Button buttonSendReset;
    @BindView(R.id.edit_reset_email)
    EditText editResetEmail;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);
        initListeners();
        return view;
    }

    public void initListeners() {
        buttonSendReset.setOnClickListener(view -> {
            Pattern emailPattern = Pattern.compile(".+@[a-z0-9]+\\.[a-z]{2,4}");
            String email = editResetEmail.getText().toString();
            if (emailPattern.matcher(email).matches()) {
                FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(getContext(), "Message was sent successfully. Check your email box.", Toast.LENGTH_LONG).show();
                        Log.d(TAG, "initListeners: Email sent.");
                        // Close dialog
                        this.dismiss();
                    } else {
                        Log.d(TAG, "initListeners: Email sending failed.");
                        Toast.makeText(getContext(), "Something goes wrong. Please check your Internet connection.", Toast.LENGTH_SHORT).show();
                    }
                });
            } else
                Toast.makeText(getContext(), "Correct your email address.", Toast.LENGTH_LONG).show();
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
