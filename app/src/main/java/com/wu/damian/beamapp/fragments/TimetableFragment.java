package com.wu.damian.beamapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.wu.damian.beamapp.MainActivity;
import com.wu.damian.beamapp.R;
import com.wu.damian.beamapp.adapters.viewholders.TaskRecyclerViewHolder;
import com.wu.damian.beamapp.core.BaseFragment;
import com.wu.damian.beamapp.database.interfaces.FirebaseCallback;
import com.wu.damian.beamapp.database.model.Task;
import com.wu.damian.beamapp.fragments.dialogs.AddTaskDialog;
import com.wu.damian.beamapp.utils.BundleKey;
import com.wu.damian.beamapp.utils.ReceiveProfessions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimetableFragment extends BaseFragment {
    @BindView(R.id.fab)
    FloatingActionButton mFab;
    ProgressBar mProgressBar;
    private Spinner mSpinnerManager;
    private RecyclerView mRecyclerView;
    private TextView mTextRecyclerIsEmpty;
    @BindView(R.id.text_profession_timetable)
    TextView mTextProfession;
    Unbinder mUnbinder;
    private String dayName;
    private String profession;
    // Display correct info for individual or profession module
    private String navDestination;
    // Firebase
    private DatabaseReference mDatabaseReference;
    private FirebaseRecyclerAdapter<Task, TaskRecyclerViewHolder> mAdapter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timetable, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        initVariables(view, inflater);
        initListeners();
        Log.d(MainActivity.TAG, "onCreateView: " + LoginFragment.user.getName());
        // Check if mRecyclerView is not empty and display notification within
        hasRecyclerData(hasData -> displayRecyclerView((boolean) hasData));
        return view;
    }

    private void initVariables(View view, @NonNull LayoutInflater inflater) {
        mProgressBar = view.findViewById(R.id.task_progress_bar);
        mTextRecyclerIsEmpty = view.findViewById(R.id.text_recycler_is_empty);
        mRecyclerView = view.findViewById(R.id.recycler_view_task);
        mSpinnerManager = view.findViewById(R.id.spinner_manager);

        // Receive notification from parent fragment
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            // Day name && profession received from prev Fragment.
            dayName = bundle.getString(BundleKey.Info.dayName);
            navDestination = bundle.getString(BundleKey.Info.navDestination);
            Log.d(MainActivity.TAG, "initVariables: bundle called dayName : " + dayName);
        }

        // If user is Admin/Manager - download extra spinners info
        if (LoginFragment.user.getProfession().equals("Admin") || LoginFragment.user.getProfession().equals("Manager")) {
            mSpinnerManager.setVisibility(View.VISIBLE);
            if (navDestination.equals("profession"))
                receiveProfessions(inflater);
            else receiveUsers(inflater);
        }
        mTextProfession.setText(LoginFragment.user.getProfession());
    }

    private void initListeners() {

        // Only Manager/Admin is capable to delete tasks and use spinner
        if (LoginFragment.user.getProfession().equals("Manager") || LoginFragment.user.getProfession().equals("Admin")) {
            deleteRecycleItem();
            mSpinnerManager.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    profession = mSpinnerManager.getSelectedItem().toString();
                    Log.d(MainActivity.TAG, "onItemSelected: spinnerContent: LISTENER : "
                            + mSpinnerManager.getSelectedItem().toString()
                            + "  profession : " + profession);
                    hasRecyclerData(hasData -> displayRecyclerView((boolean) hasData));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            mFab.setOnClickListener(view -> {
                AddTaskDialog dialog = new AddTaskDialog();
                dialog.show(getNavigation().getFragmentManagerV4(), "Add Task Dialog");
            });
        } else
            mFab.hide();
    }

    private void displayRecyclerView(boolean hasData) {

        Log.d(MainActivity.TAG, "displayRecyclerView: database : DayName : " + dayName + "  Profession : " + profession);
        // The visibility of the RecyclerView depends on whether query to DB is not empty.
        if (hasData) {
            mTextRecyclerIsEmpty.setVisibility(View.INVISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);

            // Set mRecyclerView layout
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            // Get notification from Firebase (in RecyclerView option)
            FirebaseRecyclerOptions<Task> options = new FirebaseRecyclerOptions.Builder<Task>()
                    .setQuery(mDatabaseReference, Task.class).build();
            mAdapter = new FirebaseRecyclerAdapter<Task, TaskRecyclerViewHolder>(options) {
                @Override
                protected void onBindViewHolder(@NonNull TaskRecyclerViewHolder holder, int position, @NonNull Task model) {
                    // Method set notification in each ViewHolder object (in each RecyclerView subview)
                    setTextValues(holder, model);
                }

                @NonNull
                @Override
                public TaskRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                    View view = LayoutInflater.from(getContext()).inflate(R.layout.rv_timetable,
                            viewGroup, false);
                    return new TaskRecyclerViewHolder(view);
                }

            };
            mAdapter.startListening();
            mRecyclerView.setAdapter(mAdapter);
            mProgressBar.setVisibility(View.INVISIBLE);
        } else {
            mProgressBar.setVisibility(View.INVISIBLE);
            // Make visible "RecyclerIsEmpty" TextView message
            mTextRecyclerIsEmpty.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.INVISIBLE);
        }
    }

    private void hasRecyclerData(FirebaseCallback firebaseCallback) {
        if (!(LoginFragment.user.getProfession().isEmpty() && LoginFragment.user.getName().isEmpty())) {
            // If user is not an Admin or Manager,
            if (!(LoginFragment.user.getProfession().equals("Admin") || LoginFragment.user.getProfession().equals("Manager")))
                if (navDestination.equals("profession"))
                    profession = LoginFragment.user.getProfession();
                else
                    profession = LoginFragment.user.getName();


            Log.d(MainActivity.TAG, "hasRecyclerData: spinnerContent : " + profession);

            mDatabaseReference = FirebaseDatabase.getInstance()
                    .getReference("Tasks/" + dayName + profession);

            Log.d(MainActivity.TAG, "hasRecyclerData: dayName : " + dayName);

            Query isQueryEmpty = mDatabaseReference;
            isQueryEmpty.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    // Query has notification?
                    firebaseCallback.onCallback(dataSnapshot.exists());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else {
            // Probably user fields aren't filled in the database
            mTextRecyclerIsEmpty.setText(R.string.text_timetable_no_user_info);
        }

    }

    private void deleteRecycleItem() {
        ItemTouchHelper.SimpleCallback recycleTouchHelper = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                mAdapter.getRef(viewHolder.getLayoutPosition()).removeValue();
            }
        };
        // Attaching eventRecycler with ItemTouchHelper
        new ItemTouchHelper(recycleTouchHelper).attachToRecyclerView(mRecyclerView);
    }

    private void setTextValues(@NonNull TaskRecyclerViewHolder holder, @NonNull Task model) {
        // Set task name
        holder.textTaskName.setText(model.getTaskName());
        // Set task description
        holder.textTaskDesc.setText(model.getDescription());
        // Set StartHour
        holder.textStartHour.setText(model.getHourFrom());
        // Set EndHour
        holder.textEndHour.setText(model.getHourTo());
        // Set 'done' checkbox
        holder.checkBoxTaskDone.setChecked(model.getTaskDone());
        // On user checkbox click - update database
        holder.checkBoxTaskDone.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Map<String, Object> value = new HashMap<>();
            value.put("taskDone", isChecked);
            mDatabaseReference.child(model.getHash()).updateChildren(value);
        });
    }

    private void receiveProfessions(@NonNull LayoutInflater inflater) {
        ReceiveProfessions.receiveProfessions(professions -> {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),
                    android.R.layout.simple_spinner_item, (List<String>) professions);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerManager.setAdapter(adapter);
        });
    }

    private void receiveUsers(@NonNull LayoutInflater inflater) {
        ReceiveProfessions.receiveUsers(professions -> {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),
                    android.R.layout.simple_spinner_item, (List<String>) professions);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerManager.setAdapter(adapter);
        });
    }

    public static TimetableFragment getInstance(Bundle bundle) {
        TimetableFragment tf = new TimetableFragment();
        tf.setArguments(bundle);
        return tf;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    public TimetableFragment() {
        // Required empty public constructor
    }
}
