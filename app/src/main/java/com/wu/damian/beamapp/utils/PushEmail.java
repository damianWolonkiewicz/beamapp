package com.wu.damian.beamapp.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class PushEmail {
    public static void sendEmail(String[] users, String subject, String text, Context context) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, users);
        i.putExtra(Intent.EXTRA_SUBJECT, subject + " - BeamApp");
        i.putExtra(Intent.EXTRA_TEXT, text);
        try {
            context.startActivity(Intent.createChooser(i, "Send email..."));
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "There is no email clients installed.", Toast.LENGTH_LONG).show();
        }
    }
}
