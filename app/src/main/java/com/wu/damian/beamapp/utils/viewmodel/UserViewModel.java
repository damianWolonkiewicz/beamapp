package com.wu.damian.beamapp.utils.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.wu.damian.beamapp.database.model.User;

/**
 * Class is responsible for store user info while activity, fragment etc. is destroyed.
 */
public class UserViewModel extends ViewModel {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
